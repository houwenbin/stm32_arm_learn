#ifndef __utilsH__
#define __utilsH__
#include "stm32f10x.h"

 
#define GPIOA_ODR_A  (GPIOA_BASE+0x0c)
#define GPIOA_IDR_A  (GPIOA_BASE+0x08)
#define GPIOB_ODR_A  (GPIOB_BASE+0x0c)
#define GPIOB_IDR_A  (GPIOB_BASE+0x08)
#define GPIOC_ODR_A  (GPIOC_BASE+0x0c)
#define GPIOC_IDR_A  (GPIOC_BASE+0x08)
#define GPIOD_ODR_A  (GPIOD_BASE+0x0c)
#define GPIOD_IDR_A  (GPIOD_BASE+0x08)
#define GPIOE_ODR_A  (GPIOE_BASE+0x0c)
#define GPIOE_IDR_A  (GPIOE_BASE+0x08)
 
#define BitBind(Addr,BitNum)    *((volatile unsigned long *)((Addr&0xF0000000)+0x2000000+((Addr&0xfffff)<<5)+(BitNum<<2)))
 
#define PAout(n)  BitBind(GPIOA_ODR_A,n) //ĳλ���
#define PAin(n)   BitBind(GPIOA_IDR_A,n)  //ĳλ����
#define PBout(n)  BitBind(GPIOB_ODR_A,n) 
#define PBin(n)   BitBind(GPIOB_IDR_A,n) 
#define PCout(n)  BitBind(GPIOC_ODR_A,n) 
#define PCin(n)   BitBind(GPIOC_IDR_A,n)  
#define PDout(n)  BitBind(GPIOD_ODR_A,n) 
#define PDin(n)   BitBind(GPIOD_IDR_A,n)
#define PEout(n)  BitBind(GPIOE_ODR_A,n) 
#define PEin(n)   BitBind(GPIOE_IDR_A,n) 

void GPIO_Configuration(void);
void delay(uint32_t i);
void allOff(void);
#endif
