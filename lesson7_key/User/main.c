#include "gpio_utils.h"
#include "rcc_utils.h"
#include "stm32f10x.h"
#include "sys_tick_utils.h"
#include "led_utils.h"
#include "key_utils.h"

// 主函数
int main(void)
{
    GPIO_Configuration(); //调用GPIO配置函数
	sys_tick_init(72);
	led_all_off();
	key_init();
	
    while (1) //无限循环
    {
		delay_ms(10);
		u8 key = key_scan(0);
		if(key==KEY_UP){
			led_lightn(0);
		}else if(key==KEY_DOWN){
			led_lightn(1);
		}else if(key==KEY_LEFT){
			led_lightn(2);
		}else if(key==KEY_RIGHT){
			led_lightn(3);
		}else{
			led_all_off();
		}
			
    }
}
