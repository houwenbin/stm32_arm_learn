#include "gpio_utils.h"
#include "stm32f10x.h"
#include "sys_tick_utils.h"
#include "led_utils.h"
#include "key_utils.h"
#include "exti_utils.h"

// 主函数
int main(void)
{
    GPIO_Configuration(); //调用GPIO配置函数
	sys_tick_init(72);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	custom_exti_init();

	led_all_off();
	key_init();
	
    while (1) //无限循环
    {
		delay_ms(10);
    }
}
