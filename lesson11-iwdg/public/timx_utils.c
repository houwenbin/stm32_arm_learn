#include "timx_utils.h"
#include "led_utils.h"
#include "stm32f10x.h"


/**
 * @brief  初始化定时器
 * @param  preriod: 自动重装载寄存器周期值
 * @param  prescaler: 时钟预分频数
*/
void tim4_init(u16 preriod, u16 prescaler)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
    TIM_TimeBaseStructure.TIM_Period = preriod;
    TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    // 初始化
    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
    // 中断使能
    TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
    // 使能定时器
    TIM_Cmd(TIM4, ENABLE);

    // 中断优先级NVIC设置
    NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
/**
 * @brief  初始化定时器
 * @param  ms: 定时器中断时间
*/
void tim4_init_ms(u16 ms)
{
    // 直接调用 timx_init 函数
    tim4_init(ms*2, 36000-1);
}

static u8 n=0;

void TIM4_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)
    {
        led_lightn(n);
        n++;
        if(n>9){
            n=0;
        }
        TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
    }
}

