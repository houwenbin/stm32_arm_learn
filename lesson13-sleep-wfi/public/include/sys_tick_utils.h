#ifndef __SYS_TICK_UTILS_H__
#define __SYS_TICK_UTILS_H__

#include "stm32f10x.h"
#include "core_cm3.h"

void sys_tick_init(u8 SYSCLK);
void delay_ms(u32 ms);
void delay_us(u32 nus);
void delay_second(u32 second);

#endif
