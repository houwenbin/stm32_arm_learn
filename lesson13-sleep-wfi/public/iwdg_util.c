#include "iwdg_util.h"


/**
 * @brief IWDG初始化
 * @param prer 分频数:0~7(只有低3位有效)
 * @param rlr 自动重装载寄存器值:0~0XFFF
*/
void iwdg_init(u8 prer, u16 rlr)
{
    // 开启对IWDG寄存器的写操作
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
    // 设置IWDG预分频值
    IWDG_SetPrescaler(prer);
    // 设置IWDG重装载值
    IWDG_SetReload(rlr);
    // 重装载IWDG计数器
    IWDG_ReloadCounter();
    // 使能IWDG
    IWDG_Enable();
}

/**
 * @brief 喂狗
*/
void iwdg_feed(void)
{
    IWDG_ReloadCounter();
}
