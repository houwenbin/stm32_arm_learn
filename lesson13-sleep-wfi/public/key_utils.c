#include "key_utils.h"
#include "sys_tick_utils.h"

/**
* @brief 初始化按键
*/
void key_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure; // 定义GPIO初始化结构体

    // 开启GPIOA的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    // 设置上引脚
    GPIO_InitStructure.GPIO_Pin = KEY_UP_PIN;
    // 设置输入下拉模式
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
    GPIO_Init(KEY_UP_PORT, &GPIO_InitStructure); // 初始化GPIOA

    // 开 E 时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
    // 设置下、左、右引脚
    GPIO_InitStructure.GPIO_Pin = KEY_DOWN_PIN | KEY_LEFT_PIN | KEY_RIGHT_PIN;
    // 设置输入上拉模式
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(KEY_DOWN_PORT, &GPIO_InitStructure); // 初始化GPIOE

}
/**
* @brief 读取按键值
*/
static u8 key_read(void){
   if(key_up_value == 1 || key_down_value ==0 || key_left_value ==0 || key_right_value ==0){
        delay_ms(10);
        if(key_up_value == 1){
            return KEY_UP;
        }else if(key_down_value == 0){
            return KEY_DOWN;
        }else if(key_left_value == 0){
            return KEY_LEFT;
        }else if(key_right_value == 0){
            return KEY_RIGHT;
        }
    }
    return KEY_NONE;
}
u8 last_key;
/**
 * @brief  按键扫描函数
 * @param  mode: 0 单次扫描 1: 连续扫描
 */
u8 key_scan(u8 mode)
{
    if(mode==0){
        u8 key = key_read();
        if(key != KEY_NONE){
            if(key == last_key){
                return KEY_NONE;
            }else{
                last_key = key;
                return key;
            }
        }else{
            last_key = KEY_NONE;
        }
    }else{
        return key_read();
    }
    return KEY_NONE;
}
