#include "exti_utils.h"
#include "stdio.h"
#include "sys_tick_utils.h"
#include "led_utils.h"
#include "key_utils.h"

/**
 * @brief  外部中断初始化
*/
void custom_exti_init(void) {
    // 开启AFIO时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    // GPIO 引脚映射到 EXTI 中断线
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);
    // GPIOE 映射到 EXTI 中断线
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOE, GPIO_PinSource2);
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOE, GPIO_PinSource3);
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOE, GPIO_PinSource4);

    // 配置外部中断0的优先级
    NVIC_InitTypeDef NVIC_InitStruct;
    NVIC_InitStruct.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x02;   // 抢占优先级为1
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x03;          // 响应优先级为3
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStruct);

    NVIC_InitStruct.NVIC_IRQChannel = EXTI2_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x02;   // 抢占优先级为1
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x02;          // 响应优先级为2
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStruct);
    
    NVIC_InitStruct.NVIC_IRQChannel = EXTI3_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x02;   // 抢占优先级为1
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x1;          // 响应优先级为1
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStruct);

    NVIC_InitStruct.NVIC_IRQChannel = EXTI4_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x01;   // 抢占优先级为1
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x0;          // 响应优先级为0
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStruct);

    // 初始化外部中断线路管理器（EXTI）
    EXTI_InitTypeDef EXTI_InitStruct;
    EXTI_InitStruct.EXTI_Line = EXTI_Line0;                 // 外部中断线0
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;        // 中断模式
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;    // 上升沿触发
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;                  // 使能外部中断线0
    EXTI_Init(&EXTI_InitStruct);
    
    EXTI_InitStruct.EXTI_Line = EXTI_Line2;                 // 外部中断线2
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;        // 中断模式
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;    // 下降沿触发
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;                  // 使能外部中断线0
    EXTI_Init(&EXTI_InitStruct);

    EXTI_InitStruct.EXTI_Line = EXTI_Line3;                 // 外部中断线2
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;        // 中断模式
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;    // 下降沿触发
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;                  // 使能外部中断线0
    EXTI_Init(&EXTI_InitStruct);

    EXTI_InitStruct.EXTI_Line = EXTI_Line4;                 // 外部中断线2
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;        // 中断模式
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;    // 下降沿触发
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;                  // 使能外部中断线0
    EXTI_Init(&EXTI_InitStruct);
}
/**
 * @brief  外部中断0中断服务函数
*/
void EXTI0_IRQHandler(void) {
    if (EXTI_GetITStatus(EXTI_Line0) != RESET) {
        delay_ms(10);
        if (key_up_value  == 1) {
           // led_lightn(0);
            EXTI_ClearITPendingBit(EXTI_Line0); // 清除中断标志位
            return;
        }
    }
}
/**
 * @brief  外部中断2中断服务函数
*/
void EXTI2_IRQHandler(void) {
    if (EXTI_GetITStatus(EXTI_Line2) != RESET) {
        delay_ms(10);
        if (key_left_value  == 0) {
           // led_lightn(2);
            EXTI_ClearITPendingBit(EXTI_Line2); // 清除中断标志位
            return;
        }
    }
}
/**
 * @brief  外部中断3中断服务函数
*/
void EXTI3_IRQHandler(void) {
    if (EXTI_GetITStatus(EXTI_Line3) != RESET) {
        delay_ms(10);
        if (key_down_value  == 0) {
					 __WFI();
           // led_lightn(1);
            EXTI_ClearITPendingBit(EXTI_Line3); // 清除中断标志位
            return;
        }
    }
}
/**
 * @brief  外部中断4中断服务函数
*/
void EXTI4_IRQHandler(void) {
    if (EXTI_GetITStatus(EXTI_Line4) != RESET) {
        delay_ms(20);
        if (key_right_value  == 0) {
            //led_lightn(3);
					
					  if(EXTI_GetFlagStatus(EXTI_Line4) == SET){
								printf("exit sleep\r\n");
						}
						EXTI_ClearITPendingBit(EXTI_Line4); // 清除中断标志位
            return;
        }
    }
}
