#ifndef __ADC_UTILS_H__
#define __ADC_UTILS_H__

#include "stm32f10x.h"

/**
* @brief ADCx初始化端口
*/
void ADCx_Init(void);

/**
* @param ch 通道
* @param time 读取次数
* @brief 读取ADC值
*/
u16 Get_Adc(u8 ch, u8 time);
#endif
