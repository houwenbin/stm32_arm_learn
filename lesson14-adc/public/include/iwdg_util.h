#ifndef __IWDG_UTIL_H__
#define __IWDG_UTIL_H__

#include "stm32f10x.h"
void iwdg_init(u8 prer, u16 rlr);
void iwdg_feed(void);
#endif
