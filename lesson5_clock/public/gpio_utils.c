#include "gpio_utils.h"
#include "stdint.h"
#include "stm32f10x.h"



// GPIO配置函数
void GPIO_Configuration(void)
{
    // 开启GPIOC的时钟
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;

    // 设置GPIOC的模式为推挽输出
    GPIOC->CRL = 0x33333333; // 配置低八位引脚为推挽输出模式
    GPIOC->CRH = 0x33333333; // 配置高八位引脚为推挽输出模式
}
