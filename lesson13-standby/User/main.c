#include "gpio_utils.h"
#include "stm32f10x.h"
#include "sys_tick_utils.h"
#include "led_utils.h"
#include "usart_utils.h"
#include "stdio.h"
#include "exti_utils.h"

// 主函数
int main(void)
{
	GPIO_Configuration(); // 调用GPIO配置函数
	// tick 初始化
	sys_tick_init(72);
	led_all_off();
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	USART3_Init(9600);
	printf("starting...");

	// led 初始化
	custom_led_init();
	custom_exti_init();

	int i = 0;
	
	while (1) // 无限循环
	{
		delay_ms(990);
		printf("i=%d", i);
		led_lightn(i);
		i++;
		if(i>9){
			i=0;
		}
		
	}
}
