#ifndef __STANDBY_UTILS_H__
#define __STANDBY_UTILS_H__

#include "stm32f10x.h"

/**
* @brief  进入待机模式
*/
void Standby_Enter(void);

#endif
