
#include "gpio_utils.h"
#include "rcc_utils.h"
#include "stm32f10x.h"
#include "sys_tick_utils.h"

// 主函数
int main(void)
{
    GPIO_Configuration(); //调用GPIO配置函数
	sys_tick_init(72);
	
    while (1) //无限循环
    {
			delay_ms(500);
			GPIO_ResetBits(GPIOC, GPIO_Pin_0);
			delay_ms(500);
			GPIO_SetBits(GPIOC, GPIO_Pin_0);
    }
}
