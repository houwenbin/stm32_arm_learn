#include "gpio_utils.h"
#include "stm32f10x.h"
#include "sys_tick_utils.h"
#include "led_utils.h"
#include "pwm_utils.h"

// 主函数
int main(void)
{
	// led 初始化
    custom_led_init();
	// tick 初始化
	sys_tick_init(72);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	// PWM 初始化,2K
	// 72M/500=144K 144K/72=2K
	tim3_ch1_pwm_init(500, 72-1);

	led_all_off();
	
	int i = 0;
	u8 direction=0;
    while (1) //无限循环
    {
		tim3_ch1_pwm_set_duty(i);
		if(direction==0){
			i++;
		}else{
			i--;
		}
		if(i>300){
			direction = 1;
		}else if(i<1){
			direction = 0;
		}
		delay_ms(10);
    }
}
