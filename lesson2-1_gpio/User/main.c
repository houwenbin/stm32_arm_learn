/*Include---------------------------*/
#include "stm32f10x_lib.h" //包含所有的STM32F10x库的头文件
#include <stdio.h>

// GPIO配置函数
void GPIO_Configuration(void)
{
    GPIO_InitTypeDef GPIO_InitStructure; //定义GPIO初始化结构体

    // 开启GPIOC的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    // 设置GPIOC的模式为推挽输出
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All; //选择所有的pin
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //设置输出速度为50MHz
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; //设置为推挽输出模式
    GPIO_Init(GPIOC, &GPIO_InitStructure); //初始化GPIOC
}

// 延时函数
void delay(u32 i)
{
    while (i--) //当i不为0时，持续减1，实现延时
        ;
}

// 打开指定位置的LED
void on(int position)
{
    // GPIOC->BSRR = (1 << (16 + position)); //通过设置BSRR寄存器，关闭指定位置的LED
	GPIO_ResetBits(GPIOC, GPIO_Pin_0 << position); //
}

// 关闭指定位置的LED
void off(int position)
{
    // GPIOC->BSRR = (1 << (position)); //通过设置BSRR寄存器，打开指定位置的LED
	GPIO_SetBits(GPIOC, GPIO_Pin_0 << position);
}

// 主函数
int main(void)
{
    GPIO_Configuration(); //调用GPIO配置函数

    int j;

    //GPIOC->BRR = GPIO_Pin_0; //关闭0号位的LED
		GPIO_SetBits(GPIOC, GPIO_Pin_All);
    while (1) //无限循环
    {
        for (j = 0; j < 8; j++) //遍历0到7号位
        {
            on(j); //打开j号位的LED
            delay(0xfffff); //延时
            off(j); //关闭j号位的LED
            delay(0xfffff); //延时
        }
    }
}
